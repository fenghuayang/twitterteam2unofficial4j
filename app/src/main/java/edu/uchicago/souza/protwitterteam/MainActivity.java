package edu.uchicago.souza.protwitterteam;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 *ACKNOWLEDGEMENTS:
 * How to integrate twitter: https://www.youtube.com/watch?v=_IsBi3cpvio
 * Android Twitter https://www.youtube.com/watch?v=rU2OW-Rvruk
 * Android Twitter Part 1 https://www.youtube.com/watch?v=LP3P5Ri34Tg
 * http://twitter4j.org/en/code-examples.html
 * https://dev.twitter.com/docs
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private static final String PREF_NAME ="sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN ="oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET ="oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN ="is_twitter_loggedin";
    private static final String PREF_USER_NAME ="twitter_user_name";

    public static final int WEBVIEW_REQUEST_CODE = 100;
    private ProgressDialog pDialog;
    private static Twitter twitter;
    private static RequestToken requestToken;

    private SharedPreferences sharedPreferences;

    private TextView userName;
    private EditText shareEditText;
    private View loginLayout;
    private View shareLayout;
    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //1. Initialize parameters
        initTwitterConfigs();

        //Use strict thread policy
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
                setContentView(R.layout.activity_main);


        loginLayout= (RelativeLayout)findViewById(R.id.login_layout);
        shareLayout= (LinearLayout) findViewById(R.id.share_layout);
        shareEditText=(EditText)findViewById(R.id.share_text);
        userName=(TextView)findViewById(R.id.user_name);

        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.btn_share).setOnClickListener(this);
        findViewById(R.id.btn_logout).setOnClickListener(this);

        //Make sure your app can start
        if(TextUtils.isEmpty(consumerKey)||TextUtils.isEmpty(consumerSecret)){
            Toast.makeText(this, "Twitter key not configured", Toast.LENGTH_SHORT).show();
        }

        sharedPreferences = getSharedPreferences(PREF_NAME,0);

        //Check if previously logged
        boolean isLoggedIn = sharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN,false);

        if(isLoggedIn){
            loginLayout.setVisibility(View.GONE);
            shareLayout.setVisibility(View.VISIBLE);

        //User greeting for Post screen
        String username = sharedPreferences.getString(PREF_USER_NAME, "");
        userName.setText(getResources().getString(R.string.hello_world)+" "+username);
        } else {
            loginLayout.setVisibility(View.VISIBLE);
            shareLayout.setVisibility(View.GONE);
        }

        //Build the URI
        Uri uri = getIntent().getData();

        if(uri != null && uri.toString().startsWith(callbackUrl)){
            String verifier = uri.getQueryParameter(oAuthVerifier);

            try{
                AccessToken accessToken= twitter.getOAuthAccessToken(requestToken,verifier);
                long userId = accessToken.getUserId();
                final User user = twitter.showUser(userId);
                final String username = user.getName();

                saveTwitterInfo(accessToken);

                loginLayout.setVisibility(View.GONE);
                shareLayout.setVisibility(View.VISIBLE);
                userName.setText(getString(R.string.hello_world)+" "+username);
            } catch (TwitterException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Saves information to Shared Preferences
     * @param accessToken
     */
    private void saveTwitterInfo(AccessToken accessToken) {
        long userId= accessToken.getUserId();
        User user;

        try {
            user = twitter.showUser(userId);
            String username = user.getName();

            SharedPreferences.Editor e = sharedPreferences.edit();
            e.putString(PREF_KEY_OAUTH_TOKEN,accessToken.getToken());
            //Unsafe?
            e.putString(PREF_KEY_OAUTH_SECRET,accessToken.getTokenSecret());
            e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
            e.putString(PREF_USER_NAME, username);
            e.commit();


        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens browser and makes oAuth process to log in OR if already logged in, goes to Post
     */
    private void loginToTwitter(){
        boolean isLoggedIn  = sharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN,false);
        if (!isLoggedIn){
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                requestToken=twitter.getOAuthRequestToken(callbackUrl);

                final Intent intent = new Intent (this, WebViewMain2Activity.class);
                intent.putExtra(WebViewMain2Activity.EXTRA_URL, requestToken.getAuthenticationURL());
                //TODO See onActivity Result
                startActivityForResult(intent, WEBVIEW_REQUEST_CODE);

            } catch (TwitterException e) {
                e.printStackTrace();
            }

        } else{
            loginLayout.setVisibility(View.GONE);
            shareLayout.setVisibility(View.VISIBLE);

        }
    }

    /**
     * What activity receives once Login is complete, goes to Post
     * @param requestCode
     * @param resultCode
     * @param data the Intent
     */
    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode== Activity.RESULT_OK){
            String verifier = data.getExtras().getString(oAuthVerifier);

            try {
                AccessToken accessToken = twitter.getOAuthAccessToken(requestToken,verifier);

                long userID = accessToken.getUserId();
                final User user = twitter.showUser(userID);
                String username = user.getName();

                saveTwitterInfo(accessToken);

                loginLayout.setVisibility(View.GONE);
                shareLayout.setVisibility(View.VISIBLE);

                userName.setText(MainActivity.this.getResources().getString(R.string.hello_world)+" "+username);
            } catch (TwitterException e) {
                e.printStackTrace();
            }

            super.onActivityResult(resultCode,resultCode,data);
        }
    }

    /**
     * Manages the buttons for each view
     * @param v the view
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:
                loginToTwitter();
                break;
            case R.id.btn_share:
                final String status = shareEditText.getText().toString();

                if(status.trim().length()>0){
                    new updateTwitterStatus().execute(status);

                } else {
                    Toast.makeText(this, "Tweet is empty!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_logout:
                logoutFromTwitter();
                break;
    }
}

    /**
     * 1. Helper function that helps initialize parameters
     */
    private void initTwitterConfigs(){
    consumerKey = getString(R.string.twitter_consumer_key);
    consumerSecret=getString(R.string.twitter_consumer_secret);
    callbackUrl= getString(R.string.twitter_callback);
    //TODO Check this value
    oAuthVerifier = getString(R.string.twitter_oauth_verifier);

}

    /**
     * Removes Twitter from preferences and cookies
     */
    private void logoutFromTwitter(){
        SharedPreferences.Editor e= sharedPreferences.edit();
        e.remove(PREF_KEY_OAUTH_TOKEN);
        e.remove(PREF_KEY_OAUTH_SECRET);
        e.remove(PREF_KEY_TWITTER_LOGIN);
        e.remove(PREF_USER_NAME);
        e.commit();

        //The WebView now automatically syncs cookies as necessary.
        CookieSyncManager cookieSyncManager= CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        loginLayout.setVisibility(View.VISIBLE);
        shareLayout.setVisibility(View.GONE);

    }

    /**
     * AsynchTask for Posting the tweet
     */
    class updateTwitterStatus extends AsyncTask<String,String,Void>{


        /**
         * Show it's posting
         */
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        pDialog= new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Posting to Twitter");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

        /**
         * Post with Twitter4j Twitter
         * @param params
         * @return
         */
    protected Void doInBackground(String... params) {
        String status = params[0];

        try {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            String access_token = sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
            String access_token_secret = sharedPreferences.getString(PREF_KEY_OAUTH_SECRET,"");

            AccessToken accessToken = new AccessToken(access_token,access_token_secret);

            Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

            StatusUpdate statusUpdate = new StatusUpdate(status);

            InputStream input = getResources().openRawResource(+R.mipmap.smile);
            statusUpdate.setMedia("smile.png", input);

            twitter4j.Status response = twitter.updateStatus(statusUpdate);
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        return null;
    }

        /**
         * The confirmation that we actually don't really see
         * @param aVoid
         */
    @Override
    protected void onPostExecute(Void aVoid) {
        pDialog.dismiss();
        Toast.makeText(MainActivity.this,"Posted to Twitter",Toast.LENGTH_SHORT);
        shareEditText.setText("");
    }
}
}